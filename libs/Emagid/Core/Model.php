<?php

namespace Emagid\Core;

abstract class Model implements \JsonSerializable
{

  public static $data_annotations_reserved = ['required', 'unique'];

  /**
   * @var string table name 
   */
  public static $tablename;

  /**
   * @var Array of fields . used for insert / update 
   */
  protected static $fields = array();

  /**
   * @var Array relationships with other tables
   */
  protected static $relationships = [];

  /**
   * @var holds loaded data, to support defered/  lazy loading 
   */
  protected $_data = array();

  /**
   * @var object - holds the original state of the fields, to allow reverting back to the original state, 
   *               and help us update only modified field (useful for caching and when using triggers in the DB )
   */
  protected $_original = array();

  /**
   * @var bool - whether or not the data has been loaded from DB 
   */
  private $_loaded = false;

  /**
   * @var string id field for table
   */
  public static $fld_id = "id";

  /**
  * @var string the field that determines whether a record is deleted .
  */
  public static $fld_active = 'active';

  /**
  * @var Array holds the meta information for the object.
  */
  protected static $meta = [];

  /**
   * @var int id of the current record 
   */
  private $_id = 0;

  /**
  * @var Array
  */
  public $errors = [];

  

  function __construct($params = null)
  {

    if (is_string($params) && !is_numeric($params)){

      $id = decrypt($params,static::getClassName() ); 
      
      if(is_numeric($id)) {
        $params = (int)$id;
      }
    }

    if (is_numeric($params))
    { 

      // safe to assume that the user wanted to load a single record 
      $obj = self::getItem($params);

      if($obj) {

        $this->_data = $obj->_data;
      }

      //$this->_original = $obj->_data;
    } else if (is_array($params)) {
      $this->loadProperties($params);
    }

    $this->createFields(); 
  }


  /**
  * Append all fields to the _data array, so the object is not empty 
  */
  private function createFields(){
	foreach (static::$fields as $key=>$field) {
		if (is_array($field)){
			if (!isset($this->_data[$key])) {
				$this->{$key} = null;
			}
		} else {
			if (!isset($this->_data[$field])) {
				$this->{$field} = null;
			}
		}
	}
  }

  public function __set($name, $value)
  {

    if ($name == "id")
    {
      // id is assigned, we treat it as an indicator that the object has finished loading (or attached)
      $this->_id = $value;
      //$this->_original = $this->_data;
    }

    if (!isset($this->_data[$name]) || $this->_data[$name] !== $value) {
      if (!isset($this->_original) || !isset($this->_original[$name])){
        $this->_original = isset($this->_original)?$this->_original:[];

        $this->_original[$name] = isset($this->_data[$name])? $this->_data[$name]:null ;
      }

    }


    $this->_data[$name] = $value;
  }

  public function __get($name)
  {
    // check if data was already created 
    if (isset($this->_data[$name]))
      return $this->_data[$name];

    if ($name == "id")
      return $this->_id;

    if ($name == "resources") {
      $this->_data[$name] = $this->loadKeyTableData(static::$relationships[$name]);
      return $this->_data[$name];
    }


    if (!isset($this->relationships) ) {
      return ;

    }
    foreach ($this->relationships as $relationship)
    {


      if ($relationship['name'] == $name)
      {

        if (isset($relationship['class_name']))
        { // creating a strong named object 
          $class = $relationship['class_name'];

          $key = $relationship['remote'];
          $local_val = $this->{$relationship['local']};

          if (!$local_val)
          {
            return null;
          }


          if ($relationship['relationship_type'] == 'many')
          {

            $obj = $class::getList([
                'where' => [
                  $key => $local_val
                ]
            ]);

          }
          else
          {

            $obj = $class::getItem(null,
                [
                'where' => [
                  $key => $local_val
            ]]);
          }



          $this->_data[$name] = $obj;
          $this->{$name} = $obj;


          return $obj;
        }
        else
        { // creating a generic type 
          $this->{$name} = $this->loadChildren($relationship);
          $this->_data[$name] = $this->loadChildren($relationship);
        }
      }
    }
  }

  public function jsonSerialize()
  {
    return $this->_data;
  }

  /**
   * return or create the name of the table.
   * @return string 
   */
  private static function getTableName()
  {
    $reflectionClass = new \ReflectionClass(get_called_class());
    $props = $reflectionClass->getStaticProperties();

    if (isset($props['tablename']))
      return $props['tablename'];


    $class = get_called_class ();

    if (stristr($class, '\\'))
    {
      $segments = explode('\\', $class);
      $class = $segments[count($segments) - 1];
    }


    $props['tablename'] = $class  ;

      return $class;



  }

  /**
   * load the form fields into the object after submit using GET.
   */
  public static  function loadFromGet()
  {
    $cls = get_called_class(); 
    $model = new $cls();

    $model->loadProperties($_GET);

   
    return $model;
  }

  /**
   * load the form fields into the object after submit using POST.
   */
  public static function loadFromPost()
  {
    $cls = get_called_class(); 
    $model = new $cls();
    $model->loadProperties($_POST);

    return $model;
  }
  
  /**
   * Returns the list of field names, in case the array is complex 
   */
  public function getFieldsList()
  {
    $arr = array();

    foreach ($this->fields as $key => $val)
    {

      if ($key)
        $arr[] = $key;
      else
        $arr[] = $val;
    }

    return $arr;
  }


  public function getFields()
  {
    return $this->fields; 
  }



  /**
  * Creates the query, execute it and returns the restuls for a keytable .
  *
  * @param $relationship array  keytable settings
  */
  private function loadKeyTableData($relationship){

    if (isset($relationship['class_name'])) {
      $cls = $relationship['class_name'];
      $cls_segments = explode('\\', $cls);

      $cls_tbl = $cls_segments[count($cls_segments)-1];

      $table = self::getTableName(); 

      $sql = "SELECT {$cls_tbl}_id as id FROM {$relationship['key_table']} WHERE {$table}_id = {$this->id}";

      global $emagid;

      $db = $emagid->getDb();


      $ids = $db->execute($sql) ;

      if (is_array($ids) && count($ids)){
        $str_ids = implode(',', 
            array_map(function($item) {return $item['id'];}, $ids)
          );

        return $cls::getList(['where'=>"id IN ({$str_ids}) "]);
      }      

    } else {
      throw new \Exception("Method not supported yet");

    }

  }




  

  /**
   * Insert / Update the current record 
   */
  public final function save()
  {
    global $emagid;

    if (!$this->_validate()){
      return false;
    }

    $db = $emagid->getDb();

    $vals = object_to_array($this);

    $tableName = self::getTableName();


    

    $is_insert = 0; 
    $is_update = 0; 


    // decide whether we need an INSERT or an UPDATE, and build the SQL query.
    if ($this->id == 0)
    {
      $is_insert = 1; 

      $this->run_method('beforeInsert');

      $modified = $this->getModifiedFields();

      if (isset($modified['id'])){
        unset($modified['id']);
      }

      $modified_keys = array_keys($modified);

      $keys_str = implode(',', $modified_keys);
      $vals_str = implode(',',
        array_map(function($item)
        {
          return ':' . $item;
        }, $modified_keys)
      );


      $sql = "INSERT INTO {$tableName} ({$keys_str}) VALUES($vals_str)";
    }
    else
    {
      $is_update = 1; 

      $this->run_method('beforeUpdate');

      $modified = $this->getModifiedFields();

      if (isset($modified['id'])){
        unset($modified['id']);
      }

      $modified_keys = array_keys($modified);


      if (!count($modified))
        return true;



      // array of fields changed 
      $mod_array = [];

      array_walk($modified_keys,
        function($key) use (&$mod_array)
      {
        $mod_array[] = sprintf("%s = :%s", $key, $key);
      });

      $mod_str = implode(',', $mod_array);

      $reflectionClass = new \ReflectionClass(get_called_class());
      $props = $reflectionClass->getStaticProperties();
      $sql = "UPDATE " . $tableName . " SET {$mod_str}  WHERE {$props['fld_id']} = {$this->_data[$props['fld_id']]}";
    }

    if ($db->execute($sql, $modified))
    {
      
      // Assumes this is an insert
      if ($is_insert)
      {

        // @todo :  this is only for pgsql, need to add conditions so there are no errors using mysql
        $id = $db->getLastId($tableName."_id_seq");
        
        $this->id = $id; 
        $this->_data = addIde($this->_data , static::getClassName());
        $this->{static::$fld_id} = $id;
      }

      $this->run_method($is_insert?'afterInsert':'afterUpdate');
      return true;
    }
    else
    { 
      if ($db->errors && $db->errors[0] == 0){
        return true;
      }

      if ($db->errors && count($db->errors))
      {
        $this->errors[] = $db->errors; 
      }

      if ($emagid->debug)
      {

        dd($sql,$this->errors, $this);
      }

      return false;
    }
  }


  /**
  * Initiate the validation sequence 
  */ 
  private function _validate(){
    $this->run_method('beforeValidate');
    
	static::$meta = [];
    $meta = static::getMeta(); 
    
    foreach (static::$data_annotations_reserved as $key) {
      if (isset($meta[$key]) && count($meta[$key])){
        $func = "_validate_{$key}"; 
        $this->$func($meta[$key]);
      }
    }

    foreach(static::$fields as $key=>$field){
    	if (is_array($field) && isset($field['type'])){
    		$this->_validate_type($key, $field);
    	}
    };

    $this->run_method('validate');

    return !count($this->errors);
  }


  /**
  * Check that required fields are populated.
  */
  private function _validate_required($fields){
    $this->run_method('beforeCheckRequired');
    foreach ($fields as $fld=>$meta) {
		if (trim($this->{$fld}) == ''){ 
			if (isset($meta['name'])){
				$fldName = $meta['name'];
			} else {
				$fldName = ucwords($fld);
			}
			$this->addError($fld, "The field ".$fldName." is required.");
		}
    }
  }

  private function _validate_unique($fields){
  	foreach ($fields as $fld=>$meta) {
  		$list = self::getList(['where'=>' id <> '.$this->id.' and '.$fld." = '".$this->{$fld}."'"]);
  		if (count($list)){
  			if (isset($meta['name'])){
				$fldName = $meta['name'];
			} else {
				$fldName = ucwords($fld);
			}
  			$this->addError($fld, "This ".$fldName." already exists.");
  		}
  	}
  }

  private function _validate_type($fldName, $field){
  	switch($field['type']){
  		case 'numeric':
  			if (trim($this->{$fldName}) == '') $this->{$fldName} = null;
  			if ((isset($field['required']) && $field['required'] && !is_numeric($this->{$fldName}))
  				|| (!isset($field['required']) || !$field['required']) && !is_null($this->{$fldName}) && !is_numeric($this->{$fldName})){
  				$this->addError($fldName, "Field ".(isset($field['name'])?$field['name']:ucwords($fldName))." must be numeric.");		
  			}
  			break;
  		case 'email':
  			if (trim($this->{$fldName}) == '') $this->{$fldName} = null;
  			if ((isset($field['required']) && $field['required'] || !is_null($this->{$fldName})) && !filter_var($this->{$fldName}, FILTER_VALIDATE_EMAIL) !== false){
  				$this->addError($fldName, "Invalid email address.");
  			}
  			break;
  		case 'boolean':
  			if ($this->{$fldName} === true || $this->{$fldName} == 1){
  				$this->{$fldName} = 1;
  			} else {
  				$this->{$fldName} = 0;
  			}
  			break;
  	}
  }


  private function run_method ($method_name){
    if (method_exists($this,$method_name)) {
      $this->$method_name();
    }
  }

  /**
   * get an object with only the modified fields.
   *   - will be useful for triggers, and caching.
   * @return Array  array of modified fields.
   */
  private function getModifiedFields()
  {
    $modified = [];

    $fields = self::getSelectFields();

    foreach ($this->_data as $key => $value)
    {

      if (!in_array($key, $fields))
        continue; 



      if ( ($this->id > 0 && !isset($this->_original[$key])) || $this->_original[$key] !== $value) {
        $modified[$key] = $value;
      }
    }

    return $modified;
  }

  function loadChildren($params)
  {
    global $emagid;

    $db = $emagid->getDb();

    $table = $params['table_name'];
    $local = $params['local'];
    $local_val = $this->{$local};
    $remote = $params['remote'];
    $relationship_type = $params['relationship_type'];

    if ($relationship_type == 'many')
    {
      return $db->get_results("SELECT * FROM $table WHERE $remote='$local_val'");
    }
    else
    {
      return $db->get_row("SELECT * FROM $table WHERE $remote='$local_val'");
    }
  }

  /**
   * get list from a table
   * @param $id int get     a single record by id.
   * @param $options Array  added instructions : 
   * @return Object  object representing a single result line from the DB .
   */
  public static function getItem($id, Array $options = [])
  {


    if (is_array($id) && count($options) == 0 && count($id) == 2)
    { // call was initiated from __call method 
      $options = $id[1];
      $id = $id[0];
    }

    $reflectionClass = new \ReflectionClass(get_called_class());
    $props = $reflectionClass->getStaticProperties();
    if ($id)
    {
      $options['where'][$props['fld_id']] = $id;
    }

    $list = self::getList($options);

    if (count($list))
    {
      return $list[0];
    }

    return null;
  }

  /**
   * Delete the current record 
   */
  public static function delete($id )
  {

    global $emagid;

    $db = $emagid->getDb();

    $id = static::getId($id);
    
    $table_name = self::getTableName(); 
    $active_fld = self::$fld_active ; 
    $fld_id = self::$fld_id; 

    $sql = "UPDATE {$table_name} SET {$active_fld}=0 WHERE {$fld_id}={$id}" ;
    $db->execute($sql);
  }

  /**
   * get list from a table
   * @param $params Array - conditions 
   *     $params = array(
   *            "sql" => sql statement //If this param is set, all other params would be DIfBLED
   *            "where" => array(field_name => handle, field_name => handle) //where condition for "=" and "AND"  only, NO "OR", "LIKE" or anyothers 
   *            "orderBy" => field_name 
   *            "sort" => ASC or DESC 
   *            "limit" => 10 //number
   *            "offset" => 10 //number
   *            );
   * @return Array array of objects from the db table.
   */
  public static function getList($params = array())
  {
    if (isset($params) && is_array($params) && count($params) == 1 && isset($params[0]) && is_array($params[0]))
    {
      $params = $params[0];
    }

    global $emagid;

    $db = $emagid->getDb();

    $fields = self::getSelectFields(); 

    $reflectionClass = new \ReflectionClass(get_called_class());
    $props = $reflectionClass->getStaticProperties();

    $table = self::getTableName();

    if (isset($params['sql']))
    {
      //if sql is set, just execute it without apply any other params
      $sql = $params['sql'];
    }
    else
    {
      $str_fields = implode(',', array_map(function($item) use($table) {return "{$table}.{$item}"; }, $fields));

      // joint tables were specified, we need to build the join query . 
      if (isset($params['joins'])) {
        $joins = []; 

        foreach ($params['joins'] as $key => $value) {
          $joins[] = "LEFT JOIN {$key} ON {$value}  AND {$key}.active=1";
        }

        $join_str = implode(' ', $joins); 
      } else {
        $join_str = '' ;
      }

      $sql = "SELECT {$str_fields} FROM {$table} {$join_str}";

      // apply where conditions
      if (!isset($params['where']))
        $params['where'] = [] ;
      
      $sql.=" WHERE " . self::buildWhere($params['where'], $table);
      

      // apply order and sort
      isset($params['orderBy']) ? $orderBy = $params['orderBy'] : $orderBy = $table.'.'.$props['fld_id'] . " DESC";
      $sql.= " ORDER BY {$orderBy}";


      // apply pagination
      if (isset($params['limit']) && $params['limit'] != '')
      {
        $sql.= " LIMIT " . $params['limit'];
      }

      if (isset($params['offset']))
      {
        $sql.= " OFFSET " . $params['offset'];
      }
    }//close construct sql

    $dbList = $db->getResults($sql);
    $list = [];

    if ($dbList && count($dbList)){

        $cls = get_called_class();
        array_walk($dbList, function($item) use ($cls, &$list, $props){ 
            if (is_array($item)) {
                $item = array_to_object($item);
            }

            $obj = new $cls;
            clone_into($item, $obj);
            $obj->id = $item->{$props['fld_id']};
            array_push($list, $obj);
        });
    }

    $list = array_map(function ($item) {
      $item->_data = addIde($item->_data , static::getClassName());
      return $item;

    }, $list) ; 
    
    return $list;
  }


  /**
  * Returns the meta headers, or create them if they are missing.
  */
  protected static function getMeta(){

    if (!count(static::$meta)) {
      static::parse();
    }
    
    return static::$meta;
  }

  /**
  * Parse and build the meta information for the object
  */ 
  private static function parse(){
    
    $data_annotations = []; 

    foreach (static::$fields as $fieldName=>$field) {
      if (is_array($field)) {
        foreach ($field as $key => $value) {
          if (in_array($key, static::$data_annotations_reserved) ) {
            $data_annotations[$key][$fieldName] = $field;
          }
        }
      }
    }

    static::$meta = array_merge(static::$meta, $data_annotations);

    return static::$meta;
  }


  public static function  getSelectFields(array $params = []){
    $fields = [self::$fld_id] ;

    array_walk(static::$fields, 
      function ($item, $key) use (&$fields){
        if (is_numeric($key)){
          $fields[] = $item; 
        } else {
          if(is_array($item)) {
            if (isset($item['class_name'])){
              static::$relationships[$key] = static::getRelationship($item);
            } else {
              $fields[] = $key;
            }
          }
        }
      });

    return $fields;
  }

  protected static function getRelationship($item){
    return $item ; 
  }

  public static function getCount($params = [])
  {
    return self::getCountStatic($params);
  }


  

  protected static function getClassName(){
    $name = get_called_class() ; 

    $segments = explode('\\', $name) ;

    return $segments[count($segments)-1]; 

  }

  /**
   * count rows in table.
   * @param $params Array - conditions 
   *     $params = array(
   *            "where" => array(field_name => handle, field_name => handle) //where condition for "=" and "AND"  only, NO "OR", "LIKE" or anyothers 
   *            );
   * @return int    $sql = "SELECT count(*) FROM " . self::getTableName();
   * number of records in the table 
   */
  public static function getCountStatic($params = array())
  {

    global $emagid;

    $db = $emagid->getDb();

    $sql = "SELECT count(*) FROM " . static::$tablename;



    // apply where conditions
    if (isset($params['where']))
    { // apply where conditions
      $sql.=" WHERE " . self::buildWhere($params['where']);
    }
   
    return $db->getVar($sql);
  }

  private static function buildWhere($where, $tablename = null)
  {

    $fld_active = self::$fld_active;

    if (is_null($tablename)){
        $tablename  = self::getTableName();
    }
    $arr = ["{$tablename}.{$fld_active}=1"];

    if (is_array($where))
    {
      foreach ($where as $key => $val)
      {
        if (is_array($val) && count($val) == 1) $val = $val[0];

        array_push($arr, sprintf("(%s='%s')", $key, $val));
      }
     
      return implode(" AND ", $arr);
    }
    else
    {
      return implode('', $arr).(($where == trim(''))?'':' AND '.$where);
    }
  }


  /**
  * Load properties from array 
  */
  public function loadProperties (array $props = []){

    if(isset($props['ide']) && !isset($props['id'])){
      $props['id'] = decrypt($props['ide'], static::getClassName()); 

    }

    foreach ($props as $key => $value) {
      $this->{$key} = $value;
    }

  }

  public static function getId($id){


    if (is_numeric($id))
      return $id ; 


    return decrypt($id, static::getClassName()); 
  }


  /** 
  * Add an error to the errors array .
  *
  * @param String $fld      the field that the error was called on 
  * @param String $message  the error messasge 
  */ 
  protected function addError ($fld , $message) {
    $this->errors[] = [
      'field' => $fld,
      'message' => $message
    ];
  }



  /**
  * Quick insert method 
  *
  * @param $data array - associative array where the key is the field name.
  * @return Object  
  */
  public static function insert ($data) {
    $obj = new static();
    $obj->loadProperties($data);

    $obj->id = 0;
    $obj->save();

    return $obj;
  }


  /** 
  * Quick update method .
  *
  * @param $id    Int   - record id 
  * @param $data  Array - fields to update 
  * @return Object
  */ 
  public static function update($id, $data) {
    $obj = new static($id);
    $obj->loadProperties($data);
    $obj->save();

    return $obj;
  }

  /**
  * Execute SQL statement 
  */
  protected static function execute ($sql){
    global $emagid;

    $db = $emagid->getDb();


    return $db->execute($sql);
  }


  /**
  * Retuns an associative array with the object's data 
  */
  public function dataToArray() {
    return $this->_data; 
    
  }

  	/* Returns the id of a record searching by its slug */
  	public static function getIdBySlug($slug){
	    $obj = self::getItem(null, ['where'=>"slug like '".$slug."'"]);
	    if (is_null($obj)){
	    	return 0;
	    } else {
	    	return $obj->id;
	    }
	}  

}





















