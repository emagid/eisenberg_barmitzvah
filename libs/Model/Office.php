<?php
namespace Model;

class Office extends \Emagid\Core\Model {
    static $tablename = "office";
    public static $fields = [
        'practice_id',
        'email',
        'phone',
        'address',
        'address2',
        'city',
        'state',
        'zip',
        'label'=>['required'=>true]
    ];

    public function practice(){
        return Practice::getItem($this->practice_id);
    }
}