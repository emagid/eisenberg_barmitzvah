<?php

use Twilio\Rest\Client;

class contactController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Contact | nielsen sports";
        $this->loadView($this->viewData);
    }

    public function index_post()
    {

        $sid = ACCOUNT_SID;
        $token = AUTH_TOKEN;
        $client = new Client($sid, $token);
        $from = FROM_NUMBER;

        $response = ['status'=>false,
                     'msg'=>'failed to save donation'];
        $obj = \Model\Contact::loadFromPost();
        if($obj->save()){
//          $n = new \Notification\MessageHandler('We will contact you shortly.');
//          $_SESSION["notification"] = serialize($n);
            $imgs = [];
            $gif = [];
            $image_url = null;
            if(isset($_POST['image'])){
                $img = $_POST['image'];
                $image = \Model\Snapshot_Contact::getItem($img);
                $image->contact_id = $obj->id;
                if($image->save()){
                    $response['image']=$image;
                    $image_url = 'https://'.$_SERVER['SERVER_NAME'].$image->get_image_url();
                }
            } else if(isset($_POST['gif'])){
                $_gif = $_POST['gif'];
                $gif = \Model\Gif::getItem($_gif);
                $gif->contact_id = $obj->id;
                $gif->image = str_replace('\\','/',$gif->image);
                if($gif->save()){
                    $response['gif'] = $gif;
                    $image_url = 'https://'.$_SERVER['SERVER_NAME'].$gif->get_image_url();
                }
            }

            $toNumber = $obj->phone;
            $name = $obj->name;
            
            $client->messages
            ->create(
                $toNumber,
                array(
                    "from" => $from,
                    "body" => "Thanks so much for celebrating Justin's Bar Mitzvah!! ". $image_url,
                )
            );

            // $response['email'] = $email->send();
            $response['status'] = true;
            $response['msg'] = 'Success';
            $response['contact'] = $obj;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function make_gif_post(){
        $response = ['status'=>true];
        if(isset($_POST['images'])){
            $imgs = [];
            foreach($_POST['images'] as $img){

                $image = $this->save_img($img);
                $imgs[] = $image;
            }
            $response['images'] = $imgs;

            header('Content-Type: application/json');
            echo json_encode($response);
        }
    }

    public function make_frame_post(){
        $response = ['status'=>true];
        header('Content-Type: application/json');
        if(isset($_POST['image'])){
            $image = $this->save_img($_POST['image']);
            $response['image'] = $image;
        } else {
            $response['status'] = false;
        }
        echo json_encode($response);
    }

    public function save_img_post(){
        $response = ['status'=>true];
        header('Content-Type: application/json');

        if(isset($_POST['gif'])){
            $gif = new \Model\Gif();
            $gif->frames = json_encode($_POST['images']);
            foreach($_POST['images'] as $img){
//                $_img = \Model\Snapshot_Contact::getItem($img);
                \Model\Snapshot_Contact::delete($img);
            }

            $img = $_POST['gif'];
            $img = str_replace('data:image/gif;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            $fileName = uniqid().'.gif';
            $file = UPLOAD_PATH.'Gifs'.DS.$fileName;

            if (!is_dir(UPLOAD_PATH.'Gifs'.DS)){
                mkdir(UPLOAD_PATH.'Gifs'.DS, 0777, true);
            }

            $success = file_put_contents($file, $data);


//                $gif->image = $fileName;
            $gif->image = $fileName;
            $gif->contact_id = 0;
            $gif->frames = json_encode($_POST['images']);
            if($gif->save()){
                $response['gif'] = $gif;
            } else {
                $response['status'] = false;
                $response['errors'] = $gif->errors;
                $response['msg'] = "Gif Save Failed";
            }
        } else if(isset($_POST['image'])){
            $image = $this->save_img($_POST['image']);
            $response['image']=$image;
        }
        echo json_encode($response);
    }

    public function save_img($_img, $obj = null){
        $img = $_img;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $fileName = uniqid().'.png';
        $file = UPLOAD_PATH.'Snapshots'.DS.$fileName;

        if (!is_dir(UPLOAD_PATH.'Snapshots'.DS)){
            mkdir(UPLOAD_PATH.'Snapshots'.DS, 0777, true);
        }

        $success = file_put_contents($file, $data);

        $image = new \Model\Snapshot_Contact();
        if($obj) $image->contact_id = $obj->id;
        else $image->contact_id = 0;
        $image->image = $fileName;
        $image->save();

        if($obj){
            $obj->save();
        }
        return($image);
    }
}