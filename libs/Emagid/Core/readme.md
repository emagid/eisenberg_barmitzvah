Models

using models is very simple.
all model files should go in the libs/Model folder in your project folder.

models serve as an object representation of database data. Each model will be linked to a database table, and each of these tables should have the following fields: id,active,insert_time.

you can name you model files anything you want, but always make sure to give them a name similar to the class they will contain.

For example, if you are creating a Users Model, then you will probably name your class Users. so make sure to name your file Users.php

how to initialize a model file:
	namespace Model;
	class <model_name> extends \Emagid\Core\Model {
		static $tablename = <tablename>
		public static $fields = <array of fields>;
	}

all models must be under the namespace Model.
Each model will need to have the variable $tablename, which is just a string of the name of the database table it will obtain records, update, add and delete.

The variable $fields is an array of all fields for the specified table, except id,active and insert_time.

All models must extend the class \Emagid\Core\Model to obtain the functionalities that will be used to obtain records and update records.

you can also create your own functions inside the model that can be called when you have an object created, or be called statically.

Model relationships
It is common for database tables to be associated to other tables in some ways, so you can write those associations within the model class itself.

Example:
protected $relationships = [
		[
			'name'=><relationship_name>,
			'class_name'=><class name>,//full,namespaced class name. e.g. \Model\Users
			'local'=><local id>, // field name of key being associated for this model
			'remote'=><remote id>,//field name of key associating to other model
			'relationship_type'=><single or multiple>//single returns one model object record, while multiple can return an array of model object records

		],
	];

	the relationship name is just a name you want to give to the relationship so that you can call it later.

	so if users are associated to an account, you named your relationship user_account, and you have your model object saved in the $user variable, then you can access that user's account data like this : $user->user_account-><field_name>





ORM
the ORM layer allows for Models to obtain,save and delete data in a clean manner to the database.

For example, if you have the model Users and want to get a user by a specific id, then you can use the following:
	$user = Users::getItem(<id>); // replace<id> with your id
	this will get the record from the database with the specified id and create a model object based on that data.
	you can access its data by using $user->field_name in order to get the data for that specific field name existing in the database.


you can also obtain multiple records like this:
	$users = Users::GetItem([$params]);
	this will return an array of User model objects

	the $params array is optional, and is used like this:
	$params = [
	'where'=><either string or array of key-value pairs, where the key is database field name and value is the value to search for>, // where statement for the search

	// the two below must be used together
	'orderBy'=><field_name>, // the field to order the records by
	'sort'=><either ASC or DESC>, // how to sort the records

	'limit'=><number>,//how many records to get

	'offset'=><number>,//how many records to skip

	'sql'=><query string> // your own custom query to use if other options are not enough
	];

	(Note: if you use the key 'sql', then it will run that query instead and will ignore all other options passed)

	if using your own custom query, you can join with other tables, and give fields different names, which you will be able to access those new names defined like <variable_name>-><field_name>


creating new records is easy.
	first you will have to create a new model object and assign it to a variable (lets call it $model).
	in order to set a specific field value to our model, we can do this: $model->field_name = value.

	when you are ready to save it to the database, just call on the model the save() function and it will do the rest for you.

saving records from form data.
	you can load all your post data into a model object by using loadFromPost().
	loadFromPost() will automatically create a new model object with all data already loaded and ready to be saved.

	you can also use loadFromGet() if fetching data from url (although not recommended for saving to a database)

a single record can be deleted by calling the delete static method on your model for whatever table you want to delete a record.

you can use the static method delete_list($params), where $params is an optional array with 'where' as a key and its value either a string or array, similar to the getList parameter.



